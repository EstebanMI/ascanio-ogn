<?php

namespace App\Repository;

use App\Entity\Eleve;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Eleve|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eleve|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eleve[]    findAll()
 * @method Eleve[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EleveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Eleve::class);
    }

    public function search($nom, $classe, $order = 'asc')
    {
        $em = $this->getEntityManager();
        $dql = "SELECT e FROM App\Entity\Eleve e WHERE 1=1 ";
        if ($nom) {
            $dql .= " AND e.nom LIKE :nom";
        }
        if ($classe) {
            $dql .= " AND e.classe = :classe_id";
        }

        $dql .= " ORDER BY e.nom " . $order;

        $query = $em->createQuery($dql);
        if ($nom) {
            $query->setParameter('nom', '%' . $nom . '%');
        }
        if ($classe) {
            $query->setParameter('classe_id', $classe);
        }

        return $query->getResult();
    }
}
