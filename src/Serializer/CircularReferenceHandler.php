<?php

namespace App\Serializer;

use App\Entity\Classe;
use App\Entity\Commentaire;
use App\Entity\CoursSuivi;
use App\Entity\Eleve;
use App\Entity\Matiere;
use App\Entity\Note;
use App\Entity\Professeur;
use Symfony\Component\Routing\RouterInterface;

class CircularReferenceHandler
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function __invoke($object)
    {
        switch ($object) {
            case $object instanceof Classe:
                return $this->router->generate('api_classes_detail', ['id' => $object->getId()]);
            case $object instanceof Commentaire:
                return $this->router->generate('api_commentaires_detail', ['id' => $object->getId()]);
            case $object instanceof CoursSuivi:
                return $this->router->generate('api_cours_detail', ['id' => $object->getId()]);
            case $object instanceof Eleve:
                return $this->router->generate('api_eleves_detail', ['id' => $object->getId()]);
            case $object instanceof Matiere:
                return $this->router->generate('api_matieres_detail', ['id' => $object->getId()]);
            case $object instanceof Note:
                return $this->router->generate('api_notes_detail', ['id' => $object->getId()]);
            case $object instanceof Professeur:
                return $this->router->generate('api_professeurs_detail', ['id' => $object->getId()]);
            default:
                return $object->getId();
        }
    }
}
