var eleveListe = {
    form: null,
    selectOption: null,
    target: null,
    init: function(formSelector, targetSelector) {
        this.form = $(formSelector);
        this.target = $(targetSelector);
        this.initSearchForm();
    },
    initSearchForm: function() {
        this.form.submit(function(e) {
            e.preventDefault();
            data = eleveListe.form.serializeFormJSON();
            this.showEleves(data);
        }.bind(this));
    },
    showEleves: function(formData, fromSearch = false) {
        var url = "/search/eleves";
        $('.loader').show();
        $.ajax({
            method: "GET",
            url: url,
            data: formData
        }).done(function(html){
            console.log(html);
            eleveListe.target.html(html);
            $('.loader').hide();
        }).fail(function(error) {
            $('.loader').hide();
            console.log(error);
        });
    }
}
global.eleveListe = eleveListe;
