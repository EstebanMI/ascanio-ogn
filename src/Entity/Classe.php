<?php

namespace App\Entity;

use App\Repository\ClasseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ClasseRepository::class)
 */
class Classe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show", "list", "post"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Eleve::class, mappedBy="classe", cascade={"persist"})
     * @Groups({"show", "list"})
     */
    private $eleves;

    /**
     * @ORM\OneToMany(targetEntity=CoursSuivi::class, mappedBy="classe")
     */
    private $coursSuivis;

    public function __construct()
    {
        $this->eleves = new ArrayCollection();
        $this->coursSuivis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Eleve[]
     */
    public function getEleves(): Collection
    {
        return $this->eleves;
    }

    public function addEleve(Eleve $eleve): self
    {
        if (!$this->eleves->contains($eleve)) {
            $this->eleves[] = $eleve;
            $eleve->setClasse($this);
        }

        return $this;
    }

    public function removeEleve(Eleve $eleve): self
    {
        if ($this->eleves->contains($eleve)) {
            $this->eleves->removeElement($eleve);
            // set the owning side to null (unless already changed)
            if ($eleve->getClasse() === $this) {
                $eleve->setClasse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CoursSuivi[]
     */
    public function getCoursSuivis(): Collection
    {
        return $this->coursSuivis;
    }

    public function addCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if (!$this->coursSuivis->contains($coursSuivi)) {
            $this->coursSuivis[] = $coursSuivi;
            $coursSuivi->setClasse($this);
        }

        return $this;
    }

    public function removeCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if ($this->coursSuivis->contains($coursSuivi)) {
            $this->coursSuivis->removeElement($coursSuivi);
            // set the owning side to null (unless already changed)
            if ($coursSuivi->getClasse() === $this) {
                $coursSuivi->setClasse(null);
            }
        }

        return $this;
    }
}
