<?php

namespace App\Repository;

use App\Entity\CoursSuivi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CoursSuivi|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoursSuivi|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoursSuivi[]    findAll()
 * @method CoursSuivi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursSuiviRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoursSuivi::class);
    }

    public function search($classe, $matiere, $professeur)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT e FROM App\Entity\CoursSuivi e WHERE 1=1 ";
        
        if ($classe) {
            $dql .= " AND e.classe = :classe_id";
        }
        if ($matiere) {
            $dql .= " AND e.matiere = :matiere_id";
        }
        if ($professeur) {
            $dql .= " AND e.professeur = :professeur_id";
        }

        $query = $em->createQuery($dql);

        if ($classe) {
            $query->setParameter('classe_id', $classe);
        }
        if ($matiere) {
            $query->setParameter('matiere_id', $matiere);
        }
        if ($professeur) {
            $query->setParameter('professeur_id', $professeur);
        }

        return $query->getResult();
    }

    public function findByEleve(int $id)
    {
        return $this->createQueryBuilder('co')
            ->innerJoin('co.classe', 'cl')
            ->innerJoin('cl.eleves', 'e', 'WITH', 'e.id = :eleve_id')
            ->setParameter('eleve_id', $id)
            ->getQuery()
            ->getResult();
    }
}
