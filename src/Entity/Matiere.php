<?php

namespace App\Entity;

use App\Repository\MatiereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MatiereRepository::class)
 */
class Matiere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show", "list"})
     * @Assert\NotBlank(groups={"Create"})
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=CoursSuivi::class, mappedBy="matiere")
     **/
    private $coursSuivis;

    public function __construct()
    {
        $this->coursSuivis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|CoursSuivi[]
     */
    public function getCoursSuivis(): Collection
    {
        return $this->coursSuivis;
    }

    public function addCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if (!$this->coursSuivis->contains($coursSuivi)) {
            $this->coursSuivis[] = $coursSuivi;
            $coursSuivi->setMatiere($this);
        }

        return $this;
    }

    public function removeCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if ($this->coursSuivis->contains($coursSuivi)) {
            $this->coursSuivis->removeElement($coursSuivi);
            // set the owning side to null (unless already changed)
            if ($coursSuivi->getMatiere() === $this) {
                $coursSuivi->setMatiere(null);
            }
        }

        return $this;
    }
}
