<?php

namespace App\DataFixtures;

use App\Entity\Professeur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProfesseurFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Création des professeurs
        for ($i=1; $i <= 10; $i++) {
            $prof = new Professeur();
            $prof->setNom('Prof_Nom_'.$i);
            $prof->setPrenom('Prof_Prenom_'.$i);
            $manager->persist($prof);
        }

        $manager->flush();
    }
}
