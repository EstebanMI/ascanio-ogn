<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201011140726 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE classe (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE commentaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, texte CLOB NOT NULL)');
        $this->addSql('CREATE TABLE cours_suivi (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, classe_id INTEGER NOT NULL, matiere_id INTEGER NOT NULL, professeur_id INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_1EDA43598F5EA509 ON cours_suivi (classe_id)');
        $this->addSql('CREATE INDEX IDX_1EDA4359F46CD258 ON cours_suivi (matiere_id)');
        $this->addSql('CREATE INDEX IDX_1EDA4359BAB22EE9 ON cours_suivi (professeur_id)');
        $this->addSql('CREATE TABLE eleve (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, classe_id INTEGER NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_de_naissance DATE NOT NULL)');
        $this->addSql('CREATE INDEX IDX_ECA105F78F5EA509 ON eleve (classe_id)');
        $this->addSql('CREATE TABLE matiere (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE note (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, commentaire_id INTEGER DEFAULT NULL, eleve_id INTEGER NOT NULL, cours_id INTEGER NOT NULL, valeur INTEGER NOT NULL, date_creation DATE NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CFBDFA14BA9CD190 ON note (commentaire_id)');
        $this->addSql('CREATE INDEX IDX_CFBDFA14A6CC7B2 ON note (eleve_id)');
        $this->addSql('CREATE INDEX IDX_CFBDFA147ECF78B0 ON note (cours_id)');
        $this->addSql('CREATE TABLE professeur (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_de_naissance DATE NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE classe');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE cours_suivi');
        $this->addSql('DROP TABLE eleve');
        $this->addSql('DROP TABLE matiere');
        $this->addSql('DROP TABLE note');
        $this->addSql('DROP TABLE professeur');
    }
}
