<?php

namespace App\Controller\API;

use App\Entity\Matiere;
use App\Repository\MatiereRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use App\Exception\ResourceValidationException;

/**
 * @Rest\Route("api")
 */
class MatiereController extends AbstractFOSRestController
{
    /**
     *  @var EntityManagerInterface
     */
    private $em;

    /**
     *  @var MatiereRepository
     */
    private $matiereRepository;

    public function __construct(EntityManagerInterface $em, MatiereRepository $matiereRepository)
    {
        $this->em = $em;
        $this->matiereRepository = $matiereRepository;
    }
    /**
     * @Rest\Get(
     *     path="/matieres/{id}",
     *     name="api_matieres_detail",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"show"})
     */
    public function show(Matiere $matiere)
    {
        if ($matiere) {
            return $matiere;
        }
    }

    /**
     * @Rest\Get(
     *     path="/matieres",
     *     name="api_matieres_liste",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function list()
    {
        return $this->matiereRepository->findAll();
    }

    /**
     * @Rest\Post(
     *     path="/matieres",
     *     name="api_matieres_create"
     * )
     * @Rest\View(StatusCode=201, serializerGroups={"post"})
     * @ParamConverter("matiere", converter="fos_rest.request_body",
     *      options={
     *          "validator"={"groups"="Create"}
     *      }))
     */
    public function post(Matiere $matiere, ConstraintViolationList $violations)
    {
        if (count($violations)) {
            $message = 'Le JSON contient des données invalides: ';
            foreach ($violations as $violation) {
                $message .= sprintf("Field %s: %s ", $violation->getPropertyPath(), $violation->getMessage());
            }

            throw new ResourceValidationException($message);
        }

        $this->em->persist($matiere);
        $this->em->flush();

        return $this->view($matiere, Response::HTTP_CREATED, ['Location' => $this->generateUrl('api_matieres_detail', ['id' => $matiere->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
    }

}
