<?php

namespace App\Controller\API;

use App\Entity\Professeur;
use App\Repository\ProfesseurRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use App\Exception\ResourceValidationException;

/**
 * @Rest\Route("api")
 */
class ProfesseurController extends AbstractFOSRestController
{
    /**
     *  @var EntityManagerInterface
     */
    private $em;

    /**
     *  @var ProfesseurRepository
     */
    private $professeurRepository;

    public function __construct(EntityManagerInterface $em, ProfesseurRepository $professeurRepository)
    {
        $this->em = $em;
        $this->professeurRepository = $professeurRepository;
    }
    /**
     * @Rest\Get(
     *     path="/professeurs/{id}",
     *     name="api_professeurs_detail",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"show"})
     */
    public function show(Professeur $professeur)
    {
        if ($professeur) {
            return $professeur;
        }
    }

    /**
     * @Rest\Get(
     *     path="/professeurs",
     *     name="api_professeurs_liste",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function list()
    {
        return $this->professeurRepository->findAll();
    }

    /**
     * @Rest\Post(
     *     path="/professeurs",
     *     name="api_professeurs_create"
     * )
     * @Rest\View(StatusCode=201, serializerGroups={"post"})
     * @ParamConverter("professeur", converter="fos_rest.request_body",
     *      options={
     *          "validator"={"groups"="Create"}
     *      }))
     */
    public function post(Professeur $professeur,  ConstraintViolationList $violations)
    {
        if (count($violations)) {
            $message = 'Le JSON contient des données invalides: ';
            foreach ($violations as $violation) {
                $message .= sprintf("Field %s: %s ", $violation->getPropertyPath(), $violation->getMessage());
            }

            throw new ResourceValidationException($message);
        }

        $this->em->persist($professeur);
        $this->em->flush();

        return $this->view($professeur, Response::HTTP_CREATED, ['Location' => $this->generateUrl('api_professeurs_detail', ['id' => $professeur->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
    }

}
