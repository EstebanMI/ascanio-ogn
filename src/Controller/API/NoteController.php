<?php

namespace App\Controller\API;

use App\Entity\Note;
use App\Entity\CoursSuivi;
use App\Entity\Eleve;
use App\Repository\NoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use App\Exception\ResourceValidationException;

/**
 * @Rest\Route("api")
 */
class NoteController extends AbstractFOSRestController
{
    /**
     *  @var EntityManagerInterface
     */
    private $em;

    /**
     *  @var NoteRepository
     */
    private $noteRepository;

    public function __construct(EntityManagerInterface $em, NoteRepository $noteRepository)
    {
        $this->em = $em;
        $this->noteRepository = $noteRepository;
    }
    /**
     * @Rest\Get(
     *     path="/notes/{id}",
     *     name="api_notes_detail",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"show"})
     */
    public function show(Note $note)
    {
        if ($note) {
            return $note;
        }
    }

    /**
     * @Rest\Get(
     *     path="/notes",
     *     name="api_notes_liste",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function list()
    {
        return $this->noteRepository->findAll();
    }

    /**
     * @Rest\Get(
     *     path="/eleves/{eleve_id}/notes",
     *     name="api_eleve_notes_liste",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     * @ParamConverter("eleve", options={"mapping": {"eleve_id": "id"}})
     */
    public function eleveNoteList(Eleve $eleve)
    {
        return $this->noteRepository->findBy(
            ['eleve' => $eleve->getId()],
            ['valeur' => 'asc']
        );
    }

    /**
     * @Rest\Post(
     *     path="/cours/{cours_id}/eleves/{eleve_id}/notes",
     *     name="api_notes_create"
     * )
     * @Rest\View(StatusCode=201, serializerGroups={"post"})
     * @ParamConverter("note", converter="fos_rest.request_body",
     *      options={
     *          "validator"={"groups"="Create"}
     *      }))
     * @ParamConverter("cours", options={"mapping": {"cours_id": "id"}})
     * @ParamConverter("eleve", options={"mapping": {"eleve_id": "id"}})
     */
    public function post(CoursSuivi $cours, Eleve $eleve, Note $note, ConstraintViolationList $violations)
    {
        $note->setEleve($eleve);
        $note->setCours($cours);

        if (count($violations)) {
            $message = 'Le JSON contient des données invalides: ';
            foreach ($violations as $violation) {
                $message .= sprintf("Field %s: %s ", $violation->getPropertyPath(), $violation->getMessage());
            }

            throw new ResourceValidationException($message);
        }

        $this->em->persist($note);
        $this->em->flush();

        return $this->view($note, Response::HTTP_CREATED, ['Location' => $this->generateUrl('api_notes_detail', ['id' => $note->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
    }

}
