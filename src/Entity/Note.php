<?php

namespace App\Entity;

use App\Repository\NoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 */
class Note
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show", "list", "post"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"show", "list", "post"})
     * @Assert\NotBlank(groups={"Create"})
     */
    private $valeur;

    /**
     * @ORM\OneToOne(targetEntity=Commentaire::class, cascade={"persist"})
     * @Groups({"show", "list", "post"})
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=Eleve::class, inversedBy="notes")
     * @Groups({"show", "list", "post"})
     */
    private $eleve;

    /**
     * @ORM\ManyToOne(targetEntity=CoursSuivi::class, inversedBy="notes")
     * @Groups({"show", "list", "post"})
     */
    private $cours;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    public function __construct()
    {
        $this->setDateCreation(new \DateTime);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeur(): ?int
    {
        return $this->valeur;
    }

    public function setValeur(int $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getCommentaire(): ?Commentaire
    {
        return $this->commentaire;
    }

    public function setCommentaire(?Commentaire $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getEleve(): ?Eleve
    {
        return $this->eleve;
    }

    public function setEleve(?Eleve $eleve): self
    {
        $this->eleve = $eleve;

        return $this;
    }

    public function getCours(): ?CoursSuivi
    {
        return $this->cours;
    }

    public function setCours(?CoursSuivi $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }
}
