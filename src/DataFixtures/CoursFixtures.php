<?php

namespace App\DataFixtures;

use App\Entity\Classe;
use App\Entity\Matiere;
use App\Entity\Professeur;
use App\Entity\CoursSuivi;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CoursFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Récupération des matières
        $aMatiere = $manager->getRepository(Matiere::class)->findAll();
        // Récupération des professeurs
        $aProf = $manager->getRepository(Professeur::class)->findAll();
        // Récupération des classes
        $aClasse = $manager->getRepository(Classe::class)->findAll();

        for ($i=0; $i < count($aMatiere); $i++) {
            for ($j=0; $j < 2; $j++) {
                for ($k=0; $k < 3; $k++) {
                    $cours = new CoursSuivi();
                    $cours->setMatiere($aMatiere[$i]);
                    $cours->setProfesseur($aProf[(2*$i+$j)]);
                    $cours->setClasse($aClasse[(3*$j+$k)]);
                    $manager->persist($cours);
                }
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            EleveFixtures::class,
            MatiereFixtures::class,
            ProfesseurFixtures::class,
        );
    }
}
