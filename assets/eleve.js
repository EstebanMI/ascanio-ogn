var eleve = {
    form: null,
    target: null,
    value: null,
    idEleve: null,
    init: function(formSelector, targetSelector, notesSelector, idEleve) {
        this.form = $(formSelector);
        this.target = $(targetSelector);
        this.idEleve = idEleve;
        this.notes.init('#editNotesBtn');
    },
    notes: {
        value: null,
        editBouton: null,
        init: function(editBouton) {
            $(editBouton).click(function(){
                $('.loader').show();
                $.ajax({
                   method: "GET",
                   url: "/eleves/"+eleve.idEleve+"/notes",
                   success: function(data) {
                      $('#formNoteContainer').html(data.output);
                      $('.loader').hide();
                   },
                })
            });
        }
    }
}

global.eleve = eleve;
