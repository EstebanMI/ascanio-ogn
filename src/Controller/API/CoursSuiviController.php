<?php

namespace App\Controller\API;

use App\Entity\CoursSuivi;
use App\Entity\Eleve;
use App\Entity\Matiere;
use App\Entity\Professeur;
use App\Entity\Classe;
use App\Repository\CoursSuiviRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * @Rest\Route("api")
 */
class CoursSuiviController extends AbstractFOSRestController
{
    /**
     *  @var EntityManagerInterface
     */
    private $em;

    /**
     *  @var CoursSuiviRepository
     */
    private $coursSuiviRepository;

    public function __construct(EntityManagerInterface $em, CoursSuiviRepository $coursSuiviRepository)
    {
        $this->em = $em;
        $this->coursSuiviRepository = $coursSuiviRepository;
    }
    /**
     * @Rest\Get(
     *     path="/cours/{id}",
     *     name="api_cours_detail",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"show"})
     */
    public function show(CoursSuivi $coursSuivi)
    {
        if ($coursSuivi) {
            return $coursSuivi;
        }
    }

    /**
     * @Rest\Get(
     *     path="/cours",
     *     name="api_cours_liste",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function list()
    {
        return $this->coursSuiviRepository->findAll();
    }

    /**
     * @Rest\Get(
     *     path="/search/cours",
     *     name="api_cours_searchList"
     * )
     * @Rest\QueryParam(
     *     name="classe",
     *     requirements="\d+",
     *     nullable=true,
     *     description="la classe d'élève à trouver",
     * )
     * @Rest\QueryParam(
     *     name="matiere",
     *     requirements="\d+",
     *     nullable=true,
     *     description="la matière à trouver",
     * )
     * @Rest\QueryParam(
     *     name="professeur",
     *     requirements="\d+",
     *     nullable=true,
     *     description="le professeur à trouver",
     * )
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function search(ParamFetcherInterface $paramFetcher)
    {
        $cours = $this->coursSuiviRepository->search(
            $paramFetcher->get('classe'),
            $paramFetcher->get('matiere'),
            $paramFetcher->get('professeur')
        );

        return $cours;
    }

    /**
     * @Rest\Post(
     *     path="/classes/{classe_id}/matieres/{matiere_id}/professeurs/{professeur_id}/cours",
     *     name="api_cours_create"
     * )
     * @Rest\View(StatusCode=201, serializerGroups={"post"})
     * @ParamConverter("matiere", options={"mapping": {"matiere_id": "id"}})
     * @ParamConverter("classe", options={"mapping": {"classe_id": "id"}})
     * @ParamConverter("professeur", options={"mapping": {"professeur_id": "id"}})
     */
    public function post(Classe $classe, Matiere $matiere, Professeur $professeur)
    {
        $cours = new CoursSuivi();
        $cours->setClasse($classe);
        $cours->setMatiere($matiere);
        $cours->setProfesseur($professeur);

        $this->em->persist($cours);
        $this->em->flush();

        return $this->view($cours, Response::HTTP_CREATED, ['Location' => $this->generateUrl('api_cours_detail', ['id' => $cours->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
    }

    /**
     * @Rest\Get(
     *     path="/eleves/{eleve_id}/cours",
     *     name="api_cours_byEleve"
     * )
     * @ParamConverter("eleve", options={"mapping": {"eleve_id": "id"}})
     * @Rest\View(StatusCode=200, serializerGroups={"list"})
     */
    public function getCoursEleve(Eleve $eleve)
    {
        $cours = $this->coursSuiviRepository->findByEleve(
            $eleve->getId()
        );

        return ['cours' => $cours, 'eleve' => $eleve];
    }
}
